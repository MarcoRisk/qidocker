# Misc tools for scientific websites

- Author: Marco Marchioro
- License: not available yet
- Purpose: Provide some public Docker files that are useful to generate DRF web services

## Conda base

The anaconda base on python 3.7 with pandas and numpy

### To create the docker image proceed as follows

Build the container with

```
cd condabase/condabase.0.4
docker build -t quantisland/condabase:0.4.4 .
```

Run the docker to test it

```
docker run  -e ENV1VALUE="some ENV1VALUE value" -i -t quantisland/condabase:0.4.4
```

This new version becomes the latest now

```
docker tag quantisland/condabase:0.4.4 quantisland/condabase:latest
```

Check that quantisland/condabase:latest and quantisland/condabase:0.4.4 have the same id

```
docker images | grep condabase
```

Lets push it on the remote repository

```
docker push quantisland/condabase:0.4.4
docker push quantisland/condabase:latest
```

## Scientific tools

These tools should be used for the scientific library

### To create the docker image proceed as follows

Build the container with

```
cd version1.7/
docker build -t quantisland/scitools:1.7 .
```

Run the docker to test it

```
docker run -i -t quantisland/scitools:1.7
```

This new version becomes the latest now

```
docker tag quantisland/scitools:1.7 quantisland/scitools:latest
```

Check that quantisland/scitools:latest and quantisland/scitools:1.6 have the same id

```
docker images | grep scitools
```

Lets push it on the remote repository

```
docker push quantisland/scitools:1.7
docker push quantisland/scitools:latest
```

## Web tools

These tools are needed for the DRF server

### To create the docker image proceed as follows

Build the container with

```
docker build -t web .
```

Run the docker to test it with

```
docker run -i -t -p 9111:9111  web
```

Then with a browser connect to the docker ip on port 9111 to see if it works
Remember that docker runs on a virtual machine not on localhost!

Link the remote image to the new just created by adding the new version (1.3 in this case)

```
docker tag web quantisland/web:1.3
```

Push the image on the public repository

```
docker push quantisland/web:1.3
```

Check that quantisland/web:latest and quantisland/web:1.3 have the same id

```
docker images | grep web
```

Create a new latest and push it on the public repository

```
docker tag web quantisland/web:latest
docker push quantisland/web
```

## Jupyter notebook tools

These tools are useful to install python and to run the jupyter notebooks. For now it’s almost the same as jupyter/scipy-notebook.

### To create the docker image proceed as follows

Build the container with

```
docker build -t quantisland/notebook:1.0.2 .
```

Run the docker to test it

```
docker run -i -t -p 8888:8888 quantisland/notebook:1.0.2
```

Lets push it on the remote repository

```
docker push quantisland/notebook:1.0.2
```

This new version becomes the latest now

```
docker tag quantisland/notebook:1.0.2 quantisland/notebook:latest
```

Check that quantisland/notebook:latest and quantisland/notebook:1.0.2 have the same id

```
docker images | grep notebook
```

Push all tags to the public repository

```
docker push quantisland/notebook:latest
```
