#!/usr/bin/env python

# Standard python packages
import logging
import os


def test_loggers():
    logger = logging.getLogger(__name__+".test_loggers")
    logger.debug("trial debug")
    logger.info("trial info")
    logger.warning("trial warning")
    logger.error("trial error")
    logger.critical("trial critical")


def check_env_variables():
    logger = logging.getLogger(__name__+".check_env_variables")

    env_vars = ['ENV1VALUE', 'ENV2NOVALUE']
    for ev in env_vars:
        if ev in os.environ:
            logger.info(f'Env variable {ev} has value <{os.environ[ev]}>')
        else:
            logger.warning(f'Env variable {ev} not found')


def test_data_science_tools():
    logger = logging.getLogger(__name__+".test_data_science_tools")

    logger.info("Trying to import pandas")
    import pandas as pd
    logger.info(f"pd.__file__: {pd.__file__}")

    logger.info("Trying to import numpy")
    import numpy as np
    logger.info(f"np.__file__: {np.__file__}")


def test_python_tools():
    logger = logging.getLogger(__name__+".test_python_tools")

    logger.info("Trying to import sqlalchemy")
    import sqlalchemy
    logger.info(f"sqlalchemy.__file__: {sqlalchemy.__file__}")

    logger.info("Trying to import requests")
    import requests
    logger.info(f"requests.__file__: {requests.__file__}")


def test_extra_packages():
    logger = logging.getLogger(__name__ + ".test_extra_packages")

    logger.info("Trying to import statsmodels")
    import statsmodels
    logger.info(f"statsmodels.__file__: {statsmodels.__file__}")

    logger.info("Trying to import seaborn")
    import seaborn
    logger.info(f"seaborn.__file__: {seaborn.__file__}")

    logger.info("Trying to import arch")
    import arch
    logger.info(f"arch.__file__: {arch.__file__}")

    logger.info("Trying to import pyearth")
    import pyearth
    logger.info(f"pyearth.__file__: {pyearth.__file__}")

    logger.info("Trying to import matplotlib")
    import matplotlib
    logger.info(f"matplotlib.__file__: {matplotlib.__file__}")

    logger.info("Trying to import telegram")
    import telegram
    logger.info(f"telegram.__file__: {telegram.__file__}")


def main():
    logger = logging.getLogger(__name__+".main")
    logger.info("started")

    logger.info(f"os.__file__:{os.__file__}")
    test_loggers()
    check_env_variables()
    test_data_science_tools()
    test_python_tools()
    test_extra_packages()

    logger.info("ended")


if __name__ == "__main__":
    # Setup stream handler
    logging.basicConfig(
        format="%(asctime)s[%(levelname)4.4s]%(module)s.%(funcName)s:%(message)s",
        level=logging.DEBUG
    )

    # Call main function
    main()
