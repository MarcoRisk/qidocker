#!/bin/bash --login
set -e
conda activate env
exec "$@"
